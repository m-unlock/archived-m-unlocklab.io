# dcmitype:Software a owl:Class

## Subclasses

## Annotations

|||
|-----|-----|
|<http://www.w3.org/2004/02/skos/core#definition>|A computer program in source or compiled form.|
|rdfs:comment|Examples include a C source file, MS-Windows .exe executable, or Perl script.|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[dc:hasVersion](/dc/terms/hasVersion)|A related resource that is a version, edition, or adaptation of the described resource.|1:1|xsd:string|
|[dc:identifier](/dc/terms/identifier)|The name of the sotware|1:1|xsd:string|
|[bibtex](/ontology/bibtex)|The bibtex block to cite this tool|1:1|xsd:string|
