# ppeo:observation_unit a owl:Class extends [jerm:process](/ontology/JERMOntology/process)

## Subclasses

## Annotations

|||
|-----|-----|
|<http://www.w3.org/2004/02/skos/core#definition>|An assay is a combination of a process (experiment) and a protocol.|
|rdfs:comment|Observation units are objects that are subject to instances of observation and measurement. An observation unit comprises one or more plants, and/or their environment. There can be pure environment observation units with no plants.|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[logicalPath](/ontology/logicalPath)|Logical path to folder as stored in a system|1:1|xsd:string|
|[physicalPath](/ontology/physicalPath)|Physical Path to folder as stored in a system|1:1|xsd:string|
|[sample](/ontology/sample)|Sample belonging to the observed unit|0:N|[jerm:Sample](/ontology/JERMOntology/Sample)|
