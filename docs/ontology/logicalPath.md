# logicalPath a ObjectProperty

## Domain

definition: Logical path to folder as stored in a system<br>
[jerm:Assay](/ontology/JERMOntology/Assay)

definition: Logical path to folder as stored in a system<br>
[jerm:Project](/ontology/JERMOntology/Project)

definition: Logical path to folder as stored in a system<br>
[ppeo:observation_unit](/ppeo/PPEO.owl/observation_unit)

definition: Logical path to folder as stored in a system<br>
[jerm:Investigation](/ontology/JERMOntology/Investigation)

definition: Logical path to folder as stored in a system<br>
[jerm:Study](/ontology/JERMOntology/Study)

## Range

xsd:string

## Annotations


