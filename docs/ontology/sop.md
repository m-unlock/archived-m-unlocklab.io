# sop a ObjectProperty

## Domain

definition: SOPs used to generate the assay<br>
[jerm:Assay](/ontology/JERMOntology/Assay)

definition: SOPs used to retrieve the sample<br>
[jerm:Sample](/ontology/JERMOntology/Sample)

## Range

xsd:anyURI

## Annotations


