# origin a ObjectProperty

## Domain

definition: Origin of the sample, a bioreactor, animal, soil, etc...<br>
[jerm:Sample](/ontology/JERMOntology/Sample)

## Range

[Origin](/ontology/Origin)

## Annotations


