# department a ObjectProperty

## Domain

definition: To which department the person belongs to<br>
[foaf:Person](/foaf/0.1/Person)

## Range

xsd:string

## Annotations


