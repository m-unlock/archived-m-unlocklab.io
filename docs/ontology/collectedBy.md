# collectedBy a ObjectProperty

## Domain

definition: The persons or institute who collected the specimen<br>
[jerm:Sample](/ontology/JERMOntology/Sample)

## Range

[foaf:Agent](/foaf/0.1/Agent)

## Annotations


