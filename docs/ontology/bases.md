# bases a ObjectProperty

## Domain

definition: Number of bases in the sequence file<br>
[SequenceDataSet](/ontology/SequenceDataSet)

## Range

xsd:long

## Annotations


