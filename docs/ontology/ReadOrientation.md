# readOrientation a ObjectProperty

## Domain

definition: The read orientation of the sequences<br>
[SequenceDataSet](/ontology/SequenceDataSet)

## Range

[ReadOrientation](/ontology/ReadOrientation)

## Annotations


