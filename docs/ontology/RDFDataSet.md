# RDFDataSet a owl:Class extends [jerm:Data_sample](/ontology/JERMOntology/Data_sample)

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[link](/ontology/link)|Overlapping identifier with the RDF dataset making crosslinking possible|0:N|IRI|
