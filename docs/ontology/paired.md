# paired a ObjectProperty

## Domain

definition: Paired with another data set for example paired-end sequencing<br>
[PairedSequenceDataSet](/ontology/PairedSequenceDataSet)

## Range

[PairedSequenceDataSet](/ontology/PairedSequenceDataSet)

## Annotations


