# file a ObjectProperty

## Domain

definition: Data files belonging to this assay<br>
[jerm:Assay](/ontology/JERMOntology/Assay)

## Range

[jerm:Data_sample](/ontology/JERMOntology/Data_sample)

## Annotations

|||
|-----|-----|
|<http://purl.org/dc/elements/1.1/description>|Pointing to a file object|

