# QualityControl a skos:Concept, [ComputationalAssayType](/ontology/ComputationalAssayType)

## Subclasses

## Annotations

|||
|-----|-----|
|<http://purl.org/dc/elements/1.1/description>|Assess the quality of the sequence reads|
|<http://www.w3.org/2004/02/skos/core#definition>|Do quality control checks on raw sequence data coming from high throughput sequencing pipelines. It provides a modular set of analyses which you can use to give a quick impression of whether your data has any problems of which you should be aware before doing any further analysis.|

