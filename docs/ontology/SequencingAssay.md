# SequencingAssay a owl:Class extends [jerm:experimental_assay](/ontology/JERMOntology/experimental_assay)

## Subclasses

[RNASeqAssay](/ontology/RNASeqAssay)

[DNASeqAssay](/ontology/DNASeqAssay)

[AmpliconAssay](/ontology/AmpliconAssay)

[AmpliconLibraryAssay](/ontology/AmpliconLibraryAssay)

## Annotations


## Properties

