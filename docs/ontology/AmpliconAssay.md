# AmpliconAssay a owl:Class extends [SequencingAssay](/ontology/SequencingAssay)

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[reversePrimer](/ontology/reversePrimer)|Amplicon library primer information|0:1|xsd:string|
|[forwardBarcode](/ontology/forwardBarcode)|Forward barcode sequence for an assay for a given library|0:1|xsd:string|
|[reverseBarcode](/ontology/reverseBarcode)|Reverse barcode sequence for an assay for a given library|0:1|xsd:string|
|[forwardPrimer](/ontology/forwardPrimer)|Amplicon library primer information|1:1|xsd:string|
|[library](/ontology/library)|Amplicon library associated to this amplicon assay|0:1|[AmpliconLibraryAssay](/ontology/AmpliconLibraryAssay)|
