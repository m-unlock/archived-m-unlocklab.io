# physicalPath a ObjectProperty

## Domain

definition: Physical Path to folder as stored in a system<br>
[jerm:Assay](/ontology/JERMOntology/Assay)

definition: Physical Path to folder as stored in a system<br>
[jerm:Project](/ontology/JERMOntology/Project)

definition: Physical Path to folder as stored in a system<br>
[ppeo:observation_unit](/ppeo/PPEO.owl/observation_unit)

definition: Physical Path to folder as stored in a system<br>
[jerm:Investigation](/ontology/JERMOntology/Investigation)

definition: Physical Path to folder as stored in a system<br>
[jerm:Study](/ontology/JERMOntology/Study)

## Range

xsd:string

## Annotations


