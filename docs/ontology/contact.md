# contact a ObjectProperty

## Domain

definition: Main contact person for the study performed<br>
[jerm:Study](/ontology/JERMOntology/Study)

## Range

[foaf:Person](/foaf/0.1/Person)

## Annotations


