# reference a ObjectProperty

## Domain

definition: Reference dataset often used in omics experiments<br>
[jerm:Assay](/ontology/JERMOntology/Assay)

## Range

[jerm:Data_sample](/ontology/JERMOntology/Data_sample)

## Annotations


