# ownedBy a ObjectProperty

## Domain

definition: The person who  is the owner of a process (Principal Investigator)<br>
[jerm:Project](/ontology/JERMOntology/Project)

definition: The person who  is the owner of a process (Principal Investigator)<br>
[jerm:process](/ontology/JERMOntology/process)

## Range

[foaf:Person](/foaf/0.1/Person)

## Annotations


