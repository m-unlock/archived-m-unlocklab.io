# reversePrimer a ObjectProperty

## Domain

definition: Amplicon library primer information<br>
[AmpliconAssay](/ontology/AmpliconAssay)

## Range

xsd:string

## Annotations


