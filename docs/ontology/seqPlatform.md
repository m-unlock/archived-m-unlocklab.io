# seqPlatform a ObjectProperty

## Domain

definition: The sequencing platform<br>
[SequenceDataSet](/ontology/SequenceDataSet)

## Range

[SequencingPlatform](/ontology/SequencingPlatform)

## Annotations


