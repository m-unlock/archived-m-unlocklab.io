# PairedSequenceDataSet a owl:Class extends [SequenceDataSet](/ontology/SequenceDataSet)

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[paired](/ontology/paired)|Paired with another data set for example paired-end sequencing|1:1|[PairedSequenceDataSet](/ontology/PairedSequenceDataSet)|
