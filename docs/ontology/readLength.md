# readLength a ObjectProperty

## Domain

definition: Read length<br>
[SequenceDataSet](/ontology/SequenceDataSet)

## Range

xsd:long

## Annotations


