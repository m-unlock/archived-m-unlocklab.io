# jerm:process a owl:Class

## Subclasses

[jerm:Assay](/ontology/JERMOntology/Assay)

[ppeo:observation_unit](/ppeo/PPEO.owl/observation_unit)

[jerm:Sample](/ontology/JERMOntology/Sample)

[jerm:Investigation](/ontology/JERMOntology/Investigation)

[jerm:Study](/ontology/JERMOntology/Study)

## Annotations

|||
|-----|-----|
|rdfs:comment|experimental procedures|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[ownedBy](/ontology/ownedBy)|The person who  is the owner of a process (Principal Investigator)|0:1|[foaf:Person](/foaf/0.1/Person)|
|[identifier](/ontology/identifier)|identifier|1:1|xsd:string|
|[jerm:description](/ontology/JERMOntology/description)|Description of a given section|1:1|xsd:string|
|[contactPerson](/ontology/contactPerson)||0:1|[foaf:Person](/foaf/0.1/Person)|
|[title](/ontology/title)|Title of a given section|1:1|xsd:string|
|[researcher](/ontology/researcher)||0:N|[foaf:Person](/foaf/0.1/Person)|
