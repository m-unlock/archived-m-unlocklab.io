# jerm:Investigation a owl:Class extends [jerm:process](/ontology/JERMOntology/process)

## Subclasses

## Annotations

|||
|-----|-----|
|<http://www.w3.org/2004/02/skos/core#exactMatch>|<a href="http://jermontology.org/ontology/JERMOntology#Investigation">http://jermontology.org/ontology/JERMOntology#Investigation</a>|
|rdfs:comment|A high level description of the overall area of research. This may be the overall aims of the project, as stated on your grant. If your project has several subprojects that do not share any data, you should define an investigation for each.<br><br>Example: Analysis of Central Carbon Metabolism of Sulfolobus solfataricus under varying temperatures|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[logicalPath](/ontology/logicalPath)|Logical path to folder as stored in a system|1:1|xsd:string|
|[study](/ontology/study)|Studies linked|1:N|[jerm:Study](/ontology/JERMOntology/Study)|
|[physicalPath](/ontology/physicalPath)|Physical Path to folder as stored in a system|1:1|xsd:string|
