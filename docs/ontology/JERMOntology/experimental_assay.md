# jerm:experimental_assay a owl:Class extends [jerm:Assay](/ontology/JERMOntology/Assay)

## Subclasses

[SequencingAssay](/ontology/SequencingAssay)

## Annotations

|||
|-----|-----|
|<http://purl.org/dc/elements/1.1/description>|#* The company that generated the data<br>unlock:generatedBy @foaf:Organization?;|

## Properties

