# jerm:Sample a owl:Class extends [jerm:process](/ontology/JERMOntology/process)

## Subclasses

## Annotations

|||
|-----|-----|
|<http://www.w3.org/2004/02/skos/core#definition>|Information about the sample|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[assay](/ontology/assay)||0:N|[jerm:Assay](/ontology/JERMOntology/Assay)|
|[collectionDate](/ontology/collectionDate)|The date and time on which the specimen was collected.|0:1|xsd:DateTime|
|[sop](/ontology/sop)|SOPs used to retrieve the sample|0:N|IRI|
|[origin](/ontology/origin)|Origin of the sample, a bioreactor, animal, soil, etc...|1:1|[Origin](/ontology/Origin)|
|[note](/ontology/note)|A note on the sample|0:N|xsd:string|
|[collectedBy](/ontology/collectedBy)|The persons or institute who collected the specimen|0:N|[foaf:Agent](/foaf/0.1/Agent)|
