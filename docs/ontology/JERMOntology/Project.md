# jerm:Project a owl:Class extends [jerm:Information_entity](/ontology/JERMOntology/Information_entity)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A research programme or a grant-funded programme funding the investigations.|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[logicalPath](/ontology/logicalPath)|Logical path to folder as stored in a system|1:1|xsd:string|
|[ownedBy](/ontology/ownedBy)|The person who  is the owner of a process (Principal Investigator)|0:N|[foaf:Person](/foaf/0.1/Person)|
|[identifier](/ontology/identifier)|identifier|1:1|xsd:string|
|[contactPerson](/ontology/contactPerson)||0:1|[foaf:Person](/foaf/0.1/Person)|
|[title](/ontology/title)|Title of a given section|1:1|xsd:string|
|[physicalPath](/ontology/physicalPath)|Physical Path to folder as stored in a system|1:1|xsd:string|
|[researcher](/ontology/researcher)||0:N|[foaf:Person](/foaf/0.1/Person)|
|[description](/ontology/description)|Description of a given section|1:1|xsd:string|
|[investigation](/ontology/investigation)|Investigations linked|1:N|[jerm:Investigation](/ontology/JERMOntology/Investigation)|
