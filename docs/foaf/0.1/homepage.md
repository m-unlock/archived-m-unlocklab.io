# foaf:homepage a ObjectProperty

## Domain

definition: Homepage of the organization<br>
[foaf:Organization](/foaf/0.1/Organization)

definition: Website home page of the software used<br>
[prov:SoftwareAgent](/ns/prov/SoftwareAgent)

definition: Homepage of the person (for example link to linkedin or research gate)<br>
[foaf:Person](/foaf/0.1/Person)

## Range

xsd:string

## Annotations


