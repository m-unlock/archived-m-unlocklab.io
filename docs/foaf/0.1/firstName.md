# foaf:firstName a ObjectProperty

## Domain

definition: The first name of the person<br>
[foaf:Person](/foaf/0.1/Person)

## Range

xsd:string

## Annotations


