# foaf:Agent a owl:Class

## Subclasses

[foaf:Organization](/foaf/0.1/Organization)

[prov:SoftwareAgent](/ns/prov/SoftwareAgent)

[foaf:Person](/foaf/0.1/Person)

## Annotations

|||
|-----|-----|
|rdfs:comment|The Agent class is the class of agents; things that do stuff. A well known sub-class is Person, representing people. Other kinds of agents include Organization and Group.<br><br>The Agent class is useful in a few places in FOAF where Person would have been overly specific. For example, the IM chat ID properties such as jabberID are typically associated with people, but sometimes belong to software bots.|

## Properties

