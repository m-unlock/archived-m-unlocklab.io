# foaf:mbox a ObjectProperty

## Domain

definition: mail address use  URI scheme (see RFC 2368).<br>
[foaf:Person](/foaf/0.1/Person)

## Range

xsd:anyURI

## Annotations


