# Procedure
The UNLOCK E-Infrastructure provides an infrastructure for **FAIR data** and **meta-data** storage as well as a **computational platform**. Accepted projects will get an appointed project manager with whom the possibilities and usage of the the infrastructure will be discussed.

For more information about the computational infrastructure and it's possibilities see [Workflows](../../Workflows/workflows/) (under development)
<br /><br />

### **Project Registration**

To get access to the **UNLOCK E-Infrastructure** users are asked to provide information (metadata) about their **Project** and related **Investigations** and **Studies** following the ISA standard.

This information will initially be captured off-line using a **specific preformatted Excel sheet**. This excel is empty except for Project level meta-data and a number of the predefined **Headers** for data specific meta-data. These headers should not be changed as they are part of the UNLOCK infrastructure ontology.  

As not every project is the same requiring the same headers we made a Metadata Registration Web-Form that will generate the Excel template  for specific Project, envisioned experiments and generated data. Project related information (metadata) should be entered iin this web-form. This includes: Project information, research questions, experimental setup and how the the data is or will be generated. Users are encouraged to capture as much information (metadata) as possible.

After  Web-form completion users download their custom **project specific template Excel sheet** and start registering specific meta-data of their samples and assays. The filled in Excel sheet is subsequently uploaded to the Unlock website and automatically checked for common mistakes and followed by a final manual check by the data stewrd  after which following the ISA-tab structure the data-related Project specific meta-data is automatically extracted and used to set up the data/meta-data folder structure  
<br/><br/>

##### **The Unlock Project data management structure**

![UNLOCK datamanagement](../Figures/UNLOCK_ISA_2.png)

More information and usage about the **Registration Web-Form** see [Web-Form page](../../Metadata/Webform/) 

More information of the **Excel sheet** and it's usage see [Excel sheet page](../../Metadata/Excel/)

More information about the **Unlock Ontology** see [Ontology page](../../Ontology/)
<br /><br />

### **Submitting metadata and (raw) assay data**


Submittiion of generated (raw) assay data such as sequencing data in this environment can be done via multiple ways and will be discussed with the data steward.
<br /><br />

### **Getting access to the infrastructure and your data**

under Project, users are asked to add other persons involved. Each listed person will get an account with have **read only access** to the files and (meta)data generated within the project and investigation. 

The data and metadata will be stored in a secure iRODS instance and can be accessed via different ways (e.g. file explorer and web browser and ). See [iRODS](../../iRODS/irods/) for more information and [Tutorials](../../Tutorials/Tutorials) (under development)

Next to the iRODS storage, meta-data of the provided and generated data files and other project specific meta data will also be accumulated in a RDF resource. See the Tutorials section on what this means and how it can be used in a project [Here](../../Tutorials/Tutorials) (under development).
