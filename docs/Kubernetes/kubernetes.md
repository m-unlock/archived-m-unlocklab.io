### Kubernetes


#### Adding a node to the kubernetes group

Make sure that docker is installed on the worker node. For more information see: https://docs.docker.com/engine/install/ubuntu/

To check which version is supported by Rancher please check https://rancher.com/support-maintenance-terms/all-supported-versions

*For example:* `sudo apt-get install docker-ce=5:19.03.12~3-0~ubuntu-bionic docker-ce-cli=5:19.03.12~3-0~ubuntu-bionic containerd.io`


Login into the kubernetes manager. Go to the UNLOCK cluster and click on the ... on the side and then click edit.
To add a new node to a worker node to an existing cluster scroll to the bottom and make sure that the worker option is selected. Copy the command that is shown and login onto the new worker server you would like to add.

When you have executed the command a message should appear on the rancher web interface mentioning that a node has been added.

To see the progress click on the Nodes button in the top bar when you are within the corresponding kubernetes cluster.