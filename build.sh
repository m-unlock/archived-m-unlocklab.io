#!/bin/bash
#============================================================================
#title          :Unlock Documentation
#description    :Unlock documentation
#author         :Jasper Koehorst & Bart Nijsse
#date           :2020
#version        :0.0.1
#============================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

$DIR/../ontology/build.sh

cp -r $DIR/../ontology/unlock_docs/docs/* ./docs/
cp $DIR/../ontology/unlock_docs/mkdocs_temp.yml ./