from bs4 import BeautifulSoup

import os

root = "./site"
path = os.path.join(root, "")


def main():
    all_files = []
    for path, subdirs, files in os.walk(root):
        for name in files:
            if "index.html" != name: continue
            file_path = os.path.join(path, name)
            all_files.append(file_path)
            # break
        # break

    # More efficient looping
    all_files = set(all_files)

    for file_path in all_files:
        # if file_path != "./site/index.html":continue
        print(">>>", file_path)
        html = open(file_path).read()
        parsed_html = BeautifulSoup(html)
        for element in parsed_html.body.find_all('a', attrs={'href': ''}):
            element.attrs['href'] = get_path(element, file_path, all_files)
            # print("####")

        # print(parsed_html.prettify())
        # print(file_path)
        with open(file_path, "w") as file:
            file.write(parsed_html.prettify())
        # return


def get_path(element, file_path_original, all_files):
    key = element.text
    if ":" in key:
        key = key.split(":")[-1]
    for file_path in all_files:
        if "/" + key + "/" in file_path:
            file_path_original = file_path_original.replace("index.html", "") # .replace(root, ".")
            file_path = file_path.replace("index.html","") # .replace(root, ".")
            # print(file_path)
            # print(file_path_original)
            rel_path = os.path.relpath(file_path, file_path_original)
            # print(rel_path)
            return rel_path
    return ""


#   <li class="dropdown-submenu">
#     <a tabindex="-1" href="">Process</a>
#     <ul class="dropdown-menu">


# <li >
#     <a href="../Process/"></a>
# </li>


if __name__ == '__main__':
    main()
